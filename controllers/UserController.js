const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const validateRegister = require('../validators/registerUser');
const dotenv = require('dotenv');

dotenv.config();

const userController = {};

userController.addUser =  async (req, res) => {
    try {
        const { errors, isValid } = validateRegister(req.body);

        if (!isValid) {
            return res.status(400).json(errors);
        }
        const givenUserEmail = req.body.email;
        let user = await User.findOne({ email: givenUserEmail.toLowerCase() });

        if (user) {
            throw Error("User already exists!");
        }

        const newUser = new User({
            user_name: req.body.user_name,
            password:  req.body.password,
            first_name:  req.body.first_name,
            last_name:  req.body.last_name,
            phone_number:  req.body.phone_number,
            facebook_link:  req.body.facebook_link,
            born_date:  req.body.born_date,
            is_active:  req.body.is_active,
            role:  req.body.role,
            email: req.body.email.toLowerCase(),
        });

        newUser.password = await newUser.hashPassword(req.body.password);

        await newUser.save();

        return res.status(201).json("Registration successful!");
    } catch (error) {
        return res.status(400).json("Error during add User: " + error);
    }
};

userController.getAllUsers = async (req, res) => {
    try {
        let users = await User.find();

        if (!users) {
            throw Error("Couldn't get any user!")
        }

        return res.status(200).json(users);
    } catch (error) {
        return res.status(400).json("Error during gettint userlist: " + error);
    }
};

/**
 * Logs in user
 * @param req
 * @param res
 * @returns {Promise<any|void>}
 */
userController.login = async (req, res) => {
    try {
        //Only active user filter
        let user = await User.findOne({
            user_name: req.body.user_name,
            is_active: true
        });

        if (!user) {
            throw Error("Couldn't get any user!")
        }

        let passwordIsValid = await user.comparePassword(req.body.password, user.password);

        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: "Invalid Password!"
            });
        }

        let token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            tokenKey: token
        });
    } catch (error) {
        return res.status(400).json("Error during login: " + error);
    }
};

/**
 * Delete user by given get param [:id]
 * @param req
 * @param res
 * @return {Promise<any|void>}
 */
userController.deleteUser = async (req, res) => {
    try {
        //Don't delete, set active false
        let user = await User.findOneAndUpdate({
            _id: req.params.id
        },
        {
            is_active: false
        },
        {useFindAndModify: false});

        if (!user) {
            throw Error("User with this id don't exists!")
        }

        return res.status(200).send({
            message: "User was deleted"
        });
    } catch (error) {
        return res.status(400).json("Error during delete: " + error);
    }
};

module.exports = userController;
