const ContactForm = require('../models/contact.model');
const User = require('../models/user.model');
const validateContactForm = require('../validators/contactForm');
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');

dotenv.config();

const contactController = {};

contactController.addContact = async (req, res) => {
    try {
        const { errors, isValid } = validateContactForm(req.body);

        if (!isValid) {
            throw Error("Couldn't add contactForm!");
        }
        const givenUserEmail = req.body.email;

        const newContactForm = new ContactForm({
            full_name: req.body.full_name,
            phone_number:  req.body.phone_number,
            website_url:  req.body.website_url,
            message:  req.body.message,
            email: req.body.email.toLowerCase(),
        });

        await newContactForm.save();

        return res.status(201).json("ContactForm was successfuly created!");
    } catch (error) {
        let users = await User.find({ role:  "Admin"});
        let adminEmails = "";
        users.forEach((element, index, array) => {
            adminEmails += element.email + ";";
        });
        var transporter = nodemailer.createTransport({
            service: process.env.EMAIL_SERVICE,
            auth: {
                user:  process.env.SENDER_EMAIL,
                pass:  process.env.EMAIL_PASSWORD
            }
        });

        var mailOptions = {
            from: process.env.SENDER_EMAIL,
            to: req.body.email.toLowerCase() + ";" + adminEmails,
            subject: 'ContactFrom error',
            text: "Could not save contact form"
        };

        let info = await transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });

        return res.status(400).json("Error during add ContactForm: " + error);
    }
};

contactController.getAllContactForm = async (req, res) => {
    try {
        let contactForms = await ContactForm.find();

        if (!contactForms) {
            throw Error("Couldn't get any contactForm!")
        }

        return res.status(200).json(contactForms);
    } catch (error) {
        return res.status(400).json("Error during getting contactForms: " + error);
    }
};

module.exports = contactController;
