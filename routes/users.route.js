const uRouter = require("express").Router();
const auth = require('../middlewares/auth');

const {
    getAllUsers,
    login,
    addUser,
    deleteUser,
} = require('../controllers/UserController');

uRouter.get('/all', [auth.verifyToken, auth.ensureIsAdmin], getAllUsers);
uRouter.post('/login', login);
uRouter.post('/add-user', [auth.verifyToken, auth.ensureIsAdmin], addUser);
uRouter.delete('/delete/:id', deleteUser);

module.exports = uRouter;
