const cRouter = require("express").Router();
const auth = require('../middlewares/auth');

const {
    addContact,
    getAllContactForm,
} = require('../controllers/ContactController');

cRouter.post('/add-contact', addContact);
cRouter.get('/get-contacts',[auth.verifyToken, auth.ensureIsAdmin], getAllContactForm);

module.exports = cRouter;
