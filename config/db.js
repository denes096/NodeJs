const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

mongoose
    .connect(process.env.MONGO_URL, {
        useNewUrlParser: true,
        useCreateIndex: true,
    })
    .then(() => console.log('MongoDB Connected...'))
    .catch(() => console.log('Couldn\'t connect to MongoDB...'));


