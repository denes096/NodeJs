const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    user_name: {
        type: String,
        required: true,
        unique: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    phone_number: {
      type: String,
      required: true
    },
    facebook_link: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    born_date: {
        type: Date,
        required: true
    },
    is_active: {
      type: Boolean,
      default: true
    },
    role: {
        type: String,
        enum: ['Admin', 'user'],
        required: true
    },
});

userSchema.methods.hashPassword = async password => {
    try {
        return await bcrypt.hash(password, await bcrypt.genSalt(10));
    } catch (error) {
        return error;
    }
};

userSchema.methods.comparePassword = async (password, hash) => {
    try {
        return await bcrypt.compare(password, hash);
    } catch (error) {
        return error;
    }
};

const User = mongoose.model("User", userSchema);

module.exports = User;

