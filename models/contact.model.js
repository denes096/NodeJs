const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contactSchema = new Schema({
    phone_number: {
      type: String,
      required: true
    },
    website_url: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    full_name: {
        type: String,
        required: true
    },
});

const ContactForm = mongoose.model("ContactForm", contactSchema);

module.exports = ContactForm;
