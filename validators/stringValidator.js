const Validator = require("validator");
const isEmpty = require("is-empty");

/**
 *
 * @param field field needs to be validated
 * @param value value of the field
 * @param min optional
 * @param max optional
 * @returns {{isValid: *, errors: {}}}
 */
module.exports = function validateString(field, value, min = 0, max = 255) {
    let errors = {};

    if (!value) {
        errors.password = `${ field } is required.`;
    } else if (!Validator.isLength(value, { min: min, max: max })) {
        errors.password = `${ field } must be at least ${ min }, maximum ${ max }  characters.`;
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};