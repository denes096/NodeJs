const validateString = require('./stringValidator');
const validateEmail = require('./email');
const isEmpty = require('is-empty');

module.exports = function validateRegister(inputData) {
    let errors = {};

    let validator = validateString('user_name', inputData.user_name);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateString('password', inputData.password, 7);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateString('first_name', inputData.first_name, 2);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateString('last_name', inputData.password, 2);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateEmail(inputData.email);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};