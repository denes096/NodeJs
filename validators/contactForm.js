const validateString = require('./stringValidator');
const validateEmail = require('./email');
const validateUrl = require('./url');
const isEmpty = require('is-empty');

module.exports = function validateContactForm(inputData) {
    let errors = {};

    let validator = validateString('full_name', inputData.full_name, 1);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateString('message', inputData.message, 2, 1500);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateUrl(inputData.website_url);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    validator = validateEmail(inputData.email);
    if (!validator.isValid) {
        errors = Object.assign(errors, validator.errors);
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};
