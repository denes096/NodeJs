const Validator = require("validator");
const isEmpty = require("is-empty");

/**
 * Email validator
 * @param email
 * @returns {{isValid: *, errors: {}}}
 */
module.exports = function validateUrl(url) {
    let errors = {};

    if (!url) {
        errors.email = "Url is required.";
    } else if (!Validator.isURL(url)) {
        errors.email = "Invalid url format.";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};
