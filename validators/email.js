const Validator = require("validator");
const isEmpty = require("is-empty");

/**
 * Email validator
 * @param email
 * @returns {{isValid: *, errors: {}}}
 */
module.exports = function validateEmail(email) {
    let errors = {};

    if (!email) {
        errors.email = "Email is required.";
    } else if (!Validator.isEmail(email)) {
        errors.email = "Invalid email format.";
    }

    return {
        errors,
        isValid: isEmpty(errors)
    };
};