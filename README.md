**TO RUN PROJECT**
* Fill .env required parameters
* npm install
* npm run start

**AUTHENTICATION**
* Bearer token

**API Routes**
*METHOD Description: route*

* POST login: http://127.0.0.1:3000/users/login
```
    {
        "user_name": xxxx
        "password": yyyy
    }
```

* GET Return all users: hhttp://127.0.0.1:3000/users/all
* DELETE Delete a single user by a given id: http://127.0.0.1:3000/users/delete/:id
* POST Add a single user only admin can access it: hhttp://127.0.0.1:3000/users/add-user
```
    {
        "user_name": xxxx,
        "password": yyyy,
        "first_name": cccc,
        "last_name": vvvv,
        "phone_number": 06409584593,
        "facebook_link": link,
        "email": valaki@hotmail.com,
        "born_date": yyyy-mm-dd,
        "is_active": boolean,
        "role": enum { user, admin }
    }
```
* GET all contacts only for admin: http://127.0.0.1:3000/contact/get-contacts
* POST Add single contact: http://127.0.0.1:3000/contact/add-contact
```
    {
        "full_name": name,
        "phone_number": 06705463473,
        "website_url": url,
        "message": message,
        "email": email,
    }
```
