const express = require("express");
const uRouter = require('./routes/users.route');
const cRouter = require('./routes/contact.route');
const bodyParser = require("body-parser");
const app = express();
require('./config/db');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/users', uRouter);
app.use('/contact', cRouter);

app.listen(3000, () => console.log("server is running..."));
