const jwt = require("jsonwebtoken");
const User = require('../models/user.model');
const dotenv = require('dotenv');

dotenv.config();

verifyToken = (req, res, next) => {
    let header = req.headers.authorization;
    let token = header && header.split(' ')[1];

    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        req.userId = decoded.id;
        next();
    });
};

ensureIsAdmin = async (req, res, next) => {
   let userId = req.userId;
    if (!userId) {
        return res.status(403).send({ message: "No userId provided!" });
    }

    let user = await User.findOne({ _id: userId });

    if (!user) {
        return res.status(403).send({ message: "Can not find user!" });
    }

    if (user.role == 'Admin') {
        next();
        return;
    }

    return res.status(403).send({ message: "Require Admin Role!" });
};

module.exports = { verifyToken, ensureIsAdmin };